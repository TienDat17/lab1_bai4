﻿using System;
using System.Collections.Generic;

namespace Lab01_Bai4
{
    class NhaKhoaHoc
    {
        public string hoTen { get; set; }
        public int namSinh { get; set; }
        public int bangCap { get; set; }
        public string chucVu { get; set; }
        public int baiBaoCao { get; set; }
        public int ngayCong { get; set; }
        public int bacLuong { get; set; }

        public NhaKhoaHoc()
        {

        }

        public NhaKhoaHoc(NhaKhoaHoc n)
        {
            hoTen = n.hoTen;
            namSinh = n.namSinh;
            bangCap = n.bangCap;
            chucVu = n.chucVu;
            baiBaoCao = n.baiBaoCao;
            ngayCong = n.ngayCong;
            bacLuong = n.bacLuong;
        }

        public NhaKhoaHoc(string ht, int ns, int bangCap, string cv, int baiBaoCao, int nc, int bl)
        {
            hoTen = ht;
            namSinh = ns;
            this.bangCap = bangCap;
            chucVu = cv;
            this.baiBaoCao = baiBaoCao;
            ngayCong = nc;
            bacLuong = bl;
        }

        public float tinhLuong()
        {
            return ngayCong * bacLuong;
        }

        public void NhapThongTin()
        {
            Console.Write("Nhap Ho Ten: ");
            hoTen = Console.ReadLine();
            Console.Write("Nhap Nam Sinh: ");
            namSinh = int.Parse(Console.ReadLine());
            Console.Write("Nhap So Bang Cap: ");
            bangCap = int.Parse(Console.ReadLine());
            Console.Write("Nhap Chuc Vu: ");
            chucVu = Console.ReadLine();
            Console.Write("Nhap So Bai Bao Cao Da Cong Bo: ");
            baiBaoCao = int.Parse(Console.ReadLine());
            Console.Write("Nhap So Ngay Cong: ");
            ngayCong = int.Parse(Console.ReadLine());
            Console.Write("Nhap Bac Luong: ");
            bacLuong = int.Parse(Console.ReadLine());
        }

        public void XuatThongTin()
        {
            Console.Write("Ho Ten : {0} \nNam Sinh: {1}\nSo Bang Cap: {2}\nChuc Vu: {3}\nSo Bai Bao Cao Da Cong Bo: {4}\nSo Ngay Cong: {5}\nBac Luong:{6}\nLuong: {7}\n",hoTen,namSinh,bangCap,chucVu,baiBaoCao,ngayCong,bacLuong,tinhLuong());
        }


        public void NhapDS(int n, List<NhaKhoaHoc>nk)
        {            
            for(int i = 0 ; i < n; i++)
            {
                Console.WriteLine("Nhap Thong Tin Nhan Vien Thu {0}",i+1);
                NhaKhoaHoc nkh = new NhaKhoaHoc();
                nkh.NhapThongTin();
                nk.Add(nkh);
            }
        }

        public void XuatDS(int n, List<NhaKhoaHoc> nkh)
        {
            for(int i = 0; i < n; i++)
            {
                Console.WriteLine("Thong Tin Sinh Vien Thu {0}: \n",i+1);
                nkh[i].XuatThongTin();
            }
        }
    }

    class NhaQuanLi : NhaKhoaHoc
    {
        public NhaQuanLi(NhaQuanLi nhaQuanLi)
        {
            hoTen = nhaQuanLi.hoTen;
            namSinh = nhaQuanLi.namSinh;
            bangCap = nhaQuanLi.bangCap;
            chucVu = nhaQuanLi.chucVu;
            ngayCong = nhaQuanLi.ngayCong;
            bacLuong = nhaQuanLi.bacLuong;
        }
        public NhaQuanLi(string ht, int ns, int bangCap, string cv, int nc, int bl)
        {
            hoTen = ht;
            namSinh = ns;
            this.bangCap = bangCap;
            chucVu = cv;
            ngayCong = nc;
            bacLuong = bl;
        }

        public void NhapThongTinNQL()
        {
            Console.Write("Nhap Ho Ten: ");
            hoTen = Console.ReadLine();
            Console.Write("Nhap Nam Sinh: ");
            namSinh = int.Parse(Console.ReadLine());
            Console.Write("Nhap So Bang Cap: ");
            bangCap = int.Parse(Console.ReadLine());
            Console.Write("Nhap Chuc Vu: ");
            chucVu = Console.ReadLine();
            Console.Write("Nhap So Ngay Cong: ");
            ngayCong = int.Parse(Console.ReadLine());
            Console.Write("Nhap Bac Luong: ");
            bacLuong = int.Parse(Console.ReadLine());
        }

        public void XuatThongTinNQL()
        {
            Console.Write("Ho Ten : {0} \nNam Sinh: {1}\nSo Bang Cap: {2}\nChuc Vu: {3}\nSo Ngay Cong: {4}\nBac Luong:{5}\nLuong: {6}\n", hoTen, namSinh, bangCap, chucVu, ngayCong, bacLuong, tinhLuong());
        }
    }

    class NhanVien : NhaKhoaHoc
    {
        public int Luong;
        public NhanVien(NhanVien NV)
        {
            hoTen = NV.hoTen;
            namSinh = NV.namSinh;
            bangCap = NV.bangCap;
            Luong = NV.Luong;
        }
        public NhanVien(string ht, int ns, int bangCap)
        {
            hoTen = ht;
            namSinh = ns;
            this.bangCap = bangCap;
           
        }

        public void NhapThongTinNV()
        {
            Console.Write("Nhap Ho Ten: ");
            hoTen = Console.ReadLine();
            Console.Write("Nhap Nam Sinh: ");
            namSinh = int.Parse(Console.ReadLine());
            Console.Write("Nhap So Bang Cap: ");
            bangCap = int.Parse(Console.ReadLine());
        }

        public void XuatThongTinNV()
        {
            Console.Write("Ho Ten : {0} \nNam Sinh: {1}\nSo Bang Cap: {2}\n", hoTen, namSinh, bangCap);
        }
    }

    class Test
    {
        public static void Main()
        {
            List<NhaKhoaHoc> nh = new List<NhaKhoaHoc>();
            int n;
            Console.Write("Nhap So Nhan Vien: ");
            n = int.Parse(Console.ReadLine());
            if (n <= 0)
            {
                Console.Write("So Luong Nhan Vien Phai Lon Hon 0!");
            }
            else
            {
                NhaKhoaHoc nhaKhoaHoc = new NhaKhoaHoc();
                nhaKhoaHoc.NhapDS(n, nh);
                nhaKhoaHoc.XuatDS(n, nh);
            }

        }
    }

}
